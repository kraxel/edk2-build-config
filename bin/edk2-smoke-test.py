#!/usr/bin/python3
"""
smoke test ovmf/armvirt builds using direct kernel boot
"""
import os
import sys
import glob
import argparse
import subprocess

def cmdline_qemu_x64(native):
    cmdline = [
        'qemu-system-x86_64',
        '-machine', 'q35',
        '-m', '1G',
    ]
    if native and os.path.exists('/dev/kvm'):
        cmdline += [ '-enable-kvm',
                     '-cpu', 'host' ]
    else:
        # https://github.com/tianocore/edk2/commit/bf5678b5802685e07583e3c7ec56d883cbdd5da3
        cmdline += [ '-fw_cfg', 'name=opt/org.tianocore/X-Cpuhp-Bugcheck-Override,string=yes' ]
    return cmdline

def cmdline_qemu_aa64(native):
    cmdline = [
        'qemu-system-aarch64',
        '-machine', 'virt',
        '-m', '1G',
    ]
    if native and os.path.exists('/dev/kvm'):
        cmdline += [ '-enable-kvm',
                     '-cpu', 'host' ]
    else:
        cmdline += [ '-cpu', 'cortex-a72' ]
    return cmdline

def cmdline_pflash(codefile, varsfile):
    codefmt='raw'
    varsfmt='raw'
    if codefile.endswith('.qcow2'):
        codefmt='qcow2'
    if varsfile and varsfile.endswith('.qcow2'):
        varsfmt='qcow2'
    cmdline = []
    cmdline += [ '-drive', f'if=pflash,format={codefmt},file={codefile},readonly=on' ]
    if varsfile:
        cmdline += [ '-drive', f'if=pflash,format={varsfmt},file={varsfile},snapshot=on' ]
    return cmdline

def cmdline_kernel(kernel, serial):
    cmdline = [
        '-no-reboot',
        '-kernel', kernel,
        '-append', f'panic=1 console={serial}',
    ]
    return cmdline

def cmdline_disk(image):
    cmdline = [
        '-drive', f'if=virtio,id=disk,format=qcow2,file={image}',
    ]
    return cmdline

# pylint: disable=too-many-branches,too-many-statements
def main():
    hostarch = os.uname().machine
    parser = argparse.ArgumentParser()
    parser.add_argument('--arch', dest = 'arch', type = str, default = hostarch)
    parser.add_argument('--code', dest = 'code', type = str)
    parser.add_argument('--vars', dest = 'vars', type = str)
    parser.add_argument('--kernel', dest = 'kernel', type = str)
    parser.add_argument('--disk', dest = 'disk', type = str)
    parser.add_argument('--debug', dest = 'debug', action = 'store_true', default = False)
    options = parser.parse_args()
    native = options.arch == hostarch

    if not options.code:
        raise RuntimeError('no code file given')
    if not options.disk and not options.kernel:
        if native:
            kernels = glob.glob('/lib/modules/*/vmlinuz')
            if len(kernels) == 0:
                raise RuntimeError('no kernel found')
            options.kernel = kernels[0]
        else:
            raise RuntimeError('no kernel given')

    cmdline = []
    cmdline += [ 'timeout', '--foreground', '60' ]

    if options.arch == 'x86_64':
        cmdline += cmdline_qemu_x64(native)
        serial = 'ttyS0'
    elif options.arch == 'aarch64':
        cmdline += cmdline_qemu_aa64(native)
        serial = 'ttyAMA0'
    else:
        raise RuntimeError('unsupported architecture (options.arch)')

    cmdline += [ '-vga', 'none' ]
    cmdline += [ '-net', 'none' ]
    cmdline += [ '-monitor', 'none' ]
    cmdline += [ '-parallel', 'none' ]

    if not options.debug:
        cmdline += [ '-display', 'none' ]
        cmdline += [ '-serial', 'stdio' ]
    else:
        # x64 debug mode: wire up serial + firmware.log to gtk vc tabs
        cmdline += [ '-display', 'gtk,show-tabs=on' ]
        cmdline += [ '-chardev', f'vc,id={serial}',
                     '-device', f'isa-serial,chardev={serial}' ]
        cmdline += [ '-chardev', 'vc,id=firmware.log',
                     '-device', 'isa-debugcon,iobase=0x402,chardev=firmware.log' ]

    cmdline += cmdline_pflash(options.code, options.vars)
    if options.disk:
        cmdline += cmdline_disk(options.disk)
    else:
        cmdline += cmdline_kernel(options.kernel, serial)

    print(cmdline)
    rc = subprocess.run(cmdline, check = False,
                        stdout = subprocess.PIPE,
                        stderr = subprocess.PIPE)

    print('=== stdout ===')
    output = rc.stdout.decode()
    print(output)

    if rc.stderr != b'':
        print('=== stderr ===')
        print(rc.stderr.decode())

    if options.disk:
        if not 'Hello world from startup.nsh' in output:
            print('=== FAIL: efi shell boot failure  ===')
            return 1
    else:
        if not 'Unable to mount root fs' in output:
            print('=== FAIL: kernel boot failure ===')
            return 1

    if rc.returncode != 0:
        print(f'=== FAIL: exit code {rc.returncode} ===')
        return rc.returncode

    print('=== PASS ===')
    return 0

if __name__ == '__main__':
    sys.exit(main())
