#!/bin/sh
set -ex
git clone https://github.com/tianocore/edk2.git
git clone https://github.com/tianocore/edk2-platforms.git
cd edk2
#git submodule init ArmPkg/Library/ArmSoftFloatLib/berkeley-softfloat-3
git submodule init BaseTools/Source/C/BrotliCompress/brotli
git submodule init MdePkg/Library/MipiSysTLib/mipisyst
git submodule init MdeModulePkg/Library/BrotliCustomDecompressLib/brotli
git submodule init CryptoPkg/Library/OpensslLib/openssl
git submodule init CryptoPkg/Library/MbedTlsLib/mbedtls
git submodule init RedfishPkg/Library/JsonLib/jansson
git submodule init SecurityPkg/DeviceSecurity/SpdmLib/libspdm
git submodule init MdePkg/Library/BaseFdtLib/libfdt
git submodule update --depth 1
