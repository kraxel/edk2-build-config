
helper scripts and configs for edk2 builds
==========================================


directories and scripts
-----------------------

The scripts are in `bin/`.  I suggest to symlink the scripts you want
use into `$HOME/bin`.  Available are:

 * `edk2-build.py` is main the build helper script.

 * `edk2-clone.sh` clones the edk2 repository and initializes the
   submodules needed to build `OvmfPkg` and `ArmVirtPkg`.

 * `edk2-smoke-test.py` can sanity check edk2 builds by booting them
   in qemu.

The configs I'm using myself for testbuilds are in [kraxel/](kraxel/).

The configs for distro builds are in [distro/](distro/).

The config for gitlab CI is in [gitlab-ci/](gitlab-ci/).

CI build binaries are available [here](https://kraxel.gitlab.io/edk2-build-config/).


quickstart
----------

Default config file name is `.edk2.builds` in the current directory.
Usually I symlink to the files in this repo.

 * install build dependencies.  Beside standard C language tools you
   need `nasm`, `iasl` and `libuuid-devel`.  For cross builds you need
   cross compilers installed, the script will detect and use them
   automatically.

 * run `edk2-clone.sh`

 * symlink (or copy) some config file -- for example `kraxel/x64.core`
   -- to `.edk2.builds` in the toplevel directory of the cloned edk2
   repo.  Alternatively use the `--config` switch to specify the
   configuration.

 * run `edk2-build.py -l` to list all build configs.

 * run `edk2-build.py` to build them all.

 * run `edk2-build.py -m <pattern>` to build a subset (will select
   build configs by substring match).

 * run `edk2-build.py -h` for more options.


config file
-----------

### The `[global]` section.

 * `core = DIR` location of the main edk2 directory (the one which
   carries BaseTools).  Defaults to current directory.  Can also be
   specified using the `--core DIR` switch on the command line.

 * `pkgs = DIRS` additional directories which should be checked for
   packges (space-separated).  Can also be specified using the `--pkg
   DIR` command line switch.

 * `tool = TOOLCHAIN` compiler toolchain to use.  Defaults to `GCC5`.


### The `[opts.$name]` sections.

Carries `NAME = VALUE` lines for build options (which are passed to
the edk2 build command using the `-D` command line switch).

### The `[pcds.$name]` sections.

Carries `PCD = VALUE` lines for PCDs (passed to the edk2 build command
using the `--pcd` option).

### The `[build.$name]` sections.

 * `desc = TEXT` build description.  Used for `--list`.

 * `conf = FILE` path to the DSC file to be used.

 * `arch = ARCH` architectures to build for.

 * `tgts = TARGET` targets to build for.  Defaults to `DEBUG`.

 * `opts = NAME` Name of one or more (space-separated) opts sections
   to be used (without the `opts.` prefix).

 * `pcds = NAME` Name of one or more (space-separated) pcds sections
   to be used (without the `pcds.` prefix).

 * `plat = NAME` Platform output directory (i.e. the subdirecory below
   `Build/`) used, i.e. source directory when copying builds.

 * `dest = DIR` The destination directory for builds.

 * `cpyN = SRC [ DST ]` Copy SRC file to destination directory, rename
   to DST.  DST is optional, when not specified the file is not
   renamed.  Specify multiple lines to copy more than one file.

 * `padN = DST SIZE` Pad (copied) file to the given size.
