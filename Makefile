PYLINT_OPTS	:= --py-version 3.6
PYLINT_OPTS	+= -d invalid-name
PYLINT_OPTS	+= -d global-statement
PYLINT_OPTS	+= -d missing-function-docstring	# TODO

fedora_dir := $(HOME)/distgit/edk2
rhel_9_dir := $(HOME)/rhel/srcgit/edk2/.distro/sources
qemu_dir   := $(HOME)/projects/qemu/roms

default:
	@echo "targets: clean fedora rhel-9 fat.qcow2"

fat.qcow2:
	qemu-img convert -f vvfat -O qcow2 -o cluster_size=4096 fat:fat $@

lint pylint:
	pylint $(PYLINT_OPTS) bin/*.py

clean:
	rm -f *~ */*~
	rm -f fat.qcow2

fedora:
	cp bin/edk2-build.py $(fedora_dir)
	cp distro/fedora $(fedora_dir)/edk2-build.fedora
	cp distro/fedora.platforms $(fedora_dir)/edk2-build.fedora.platforms
	cp distro/rhel-9 $(fedora_dir)/edk2-build.rhel-9
	cp distro/rhel-10 $(fedora_dir)/edk2-build.rhel-10

el9 rhel-9:
	cp bin/edk2-build.py $(rhel_9_dir)
	cp distro/rhel-9 $(rhel_9_dir)/edk2-build.rhel-9

el10 rhel-10:
	cp bin/edk2-build.py $(rhel_9_dir)
	cp distro/rhel-10 $(rhel_9_dir)/edk2-build.rhel-10

qemu:
	cp bin/edk2-build.py $(qemu_dir)
	cp distro/qemu $(qemu_dir)/edk2-build.config
